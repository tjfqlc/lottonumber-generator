#define PI 		3.141592		// 원주율
#define TWOPI 	(3.141592 * 2.0)	// 원주율의 2배
#define MAX_INT 	2147483647	// 최대정수
#define EOF 	(-1)		// 파일의 끝표시
#define MAX_STUDENTS 	2000	// 최대 학생수
#define EPS 	1.0e-9		// 실수의 계산 한계
#define DIGITS 	"0123456789"	// 문자 상수 정의
#define BRACKET 	"(){}[]"		// 문자 상수 정의
#define getchar() 	getc(stdin)		// stdio.h에 정의
#define putchar() 	putc(stdout)	// stdio.h에 정의
#include<stdio.h>
#include<Windows.h>
#define AND		&&
#define OR 		||
#define NOT		!
#define IS		==
#define ISNOT	!=
int search(int list[], int n, int key) {
	int i = 0;

	while (i < n AND list[i] ISNOT key) i++; \
	if (i IS n) return -1;
	else return i;
}


int main() {
	int m[] = { 1,2,3,4,5,6,7 };
	int i;
	INPUT("%d", &i);
	printf("position of 5 in arr= %d\n",search(m,sizeof(m)/sizeof(m[0]),i));
	Sleep(5000);
	return 0;
}

