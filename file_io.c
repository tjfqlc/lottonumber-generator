#include<stdio.h>
#include<Windows.h>

int main() {
	FILE *fp;
	char buffer[100];

	fp = fopen("sample.txt", "wt");
	fputs("ABCDEFGHIJKLMNOPQRSTUVWXYZ", fp);
	fclose(fp);
	
	fp = fopen("sample.txt", "rt");

	fseek(fp, 3, SEEK_SET);
	printf("fseek(fp,3,seek_set)=%c\n", fgetc(fp));
	fseek(fp, -2, SEEK_END);
	printf("fseek(fp,-2,seek_set)=%c\n", fgetc(fp));

	fclose(fp);

	Sleep(5000);

	return 0;
}