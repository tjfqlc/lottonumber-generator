#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<stdio.h>
#include<Windows.h>
void generator(int num[]);
void fileopen();
void filewrite(int gold);
void menu();

int main() {
	menu();
}
void generator(int num[]){//로또번호 생성기
	srand(__rdtsc()); // 난수 생성을 위해 srand로 초기화 seed로 time 함수를 쓰면'초'단위이기때문에 컴파일이 빨리끝나면 같은수가 나와버림, 고로 클럭단위로 seed를 줄수있게 rdtsc사용
	for (int i = 0; i < 6; i++){
		num[i] = (rand() % 45)+1;// 난수생성
		for (int j = 0; j < i; j++)//중복체크 2중for문
		{
			if (num[j] == num[i]) i--;
		}
	}
	printf("생성된 로또번호는:");
	for (int i = 0; i < 6; i++) {
		printf("%d ", num[i]);
	}
	puts("\n");
}
void filewrite(int gold) {//파일에 생성된 로또번호를 적는 함수
	int lottonum[6]; //생성 번호 저장배열
	FILE* fp = fopen("lottonumber.txt", "w");//로또 번호를 저장할 파일을 w모드로 open
	printf("%d개 생성\n",gold);
	for (int i = 0; i < gold; i++)
	{
		generator(lottonum);
		for (int j = 0; j < 6; j++)//배열의 숫자를 파일에 작성
		{
			fprintf(fp, "%d ", lottonum[j]);
		}
		fprintf(fp, "\n");//회차는 줄바꿈으로 구분
	}
	fclose(fp);//file close
}
void fileopen() {//파일에서 숫자를 가져와 빈도수를 조사하는 함수
	int frequency[46] = { 0, };//로또번호의 빈도수 체크 배열(1-45 인덱스 사용)
	int max;//특정 번호가 나온수 저장
	int maxindex;//번호의 인덱스 저장
	int buf;//txt파일에서 숫자를 가져올 버퍼
	FILE* fp = fopen("lottonumber.txt", "r");
	while (EOF != fscanf(fp, "%d", &buf))  // 파일의 끝이 아니라면 (fsanf의 값이 eof인지 판단)
	{
		frequency[buf]++; //45개의 숫자중 나온 숫자에 해당하는 인덱스의 값에 1을더하여 빈도수 측정
	}
	for (int j = 0; j < 45; j++)
	{
		max = 0;//max를 0으로 초기화(초기값이 쓰레기값이면 60번줄에서 비교가 안되므로)
		for (int i = 1; i < 46; i++)
		{
			if (frequency[i] > max) {//빈도수가 저장되있는 배열에서 최댓값을 찾는다.
				max = frequency[i];
				maxindex = i;//빈도수가 가장높았던 인덱스를 저장(인덱스가 로또 번호이므로)
			}
		}
		printf("%d 번 나온 %d를 넣으세요!\n", max, maxindex);
		frequency[maxindex] = 0;//그다음으로 빈도수가 높았던 번호를 찾기위해 최빈도수 의 인덱스의 값을 0으로 변경
	}
	fclose(fp);
}
void menu() {//메뉴함수
	int flag_menu = 1;
	while (1)
	{ 
		printf("--------------------반갑습니다, 로또 번호 생성기입니다.-------------------------\n");
		printf("1번: 종료\n2번:로또 번호 및 파일 생성\n3번:기존 로또 파일 기반 숫자 추천\n4번:로또 확률 설명듣기\n");
		printf("선택하세요:");
		scanf("%d", &flag_menu);
		if (flag_menu == 1) return 0;
		else if (flag_menu == 2) {
			int gold = 0;
			printf("(1회 100원)돈을 투입하세요:");
			scanf("%d", &gold);
			if (gold % 100 != 0) printf("거스름돈:%d원\n", (gold % 100)); //모듈러 연산을 통해 거스름돈 출력
			gold = gold-(gold % 100);//받아야 할 돈만 계산
			filewrite(gold/100);//생성할 로또 개수 전달(gold/가격)
		}
		else if (flag_menu == 3) fileopen();
		else if (flag_menu == 4) printf("로또는 매번 시행의 확률이 같은 독립시행입니다. 즉 로또번호를 예측하는일은 다음에 \n나올 주사위 번호를 예측하는것과 같은 무의미 한 일입니다^^!!.\n");
		else printf("다시 입력해주세요");
	}
}